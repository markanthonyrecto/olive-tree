'use strict';

var olivetreeMap = {
	target: '',
	key: document.querySelector('meta[name="google-key"]').getAttribute('content'),
	location: {
		lat: 1.3183347267362207,
		lng: 103.84367206224243,
	},
	main: '',
	marker: '',
	init: function() {
		olivetreeMap.target = document.querySelector('.com_olive-tree-map');

		// for map
		olivetreeMap.main = new google.maps.Map(olivetreeMap.target, {
			zoom: 19,
			center: olivetreeMap.location
		});

		// for marker
		olivetreeMap.marker = new google.maps.Marker({position: olivetreeMap.location, icon: 'assets/img/location-pin.png', map: olivetreeMap.main});
	}
}

const olivetree = {
	preloader: true,
	init: function() {
		// for mobile
		if(olivetree.isMobile()) {
			$('html').addClass('bp-touch');
		}

		// for preloader
		if(olivetree.getUrlVars().preloader == 'false') {
			olivetree.preloader = false;
		}
		if(olivetree.preloader) {
			const _preloader = document.querySelector('.com_preloader');
			if(_preloader) {
				olivetree.preloader = new olivetree.olivePreloader({target: _preloader});
			} else {
				olivetree.preloader = false;
			}
		} else {
			$('.com_preloader').remove();
		}

		// for header
		olivetree.header.init();

		// for general resize
		olivetree.resizeFn(function() {
			olivetree.resize();
		});

		// for ready
		$(document).ready(function() {
			olivetree.dispatchEvent(window, 'resize');

			// preloader upon ready
			if(olivetree.preloader) {
				// preload the image backgrounds
				const _assets = [];

				const _getImage = function(img) {
					if(img != 'none') {
						img = img.replace('url("', '')
						img = img.replace('")', '');

						const _img = document.createElement('img');
						_img.src = img;

						return _img;
					} else {
						return false;
					}
				}

				$('.container-fluid').each(function() {
					let _bgImage = _getImage(window.getComputedStyle(this).backgroundImage);

					if(_bgImage) {
						_assets.push(_bgImage);
					}
				});

				$('.com_olive-tree-bg').each(function() {
					let _bgImage = _getImage(window.getComputedStyle(this, ':before').backgroundImage);

					if(_bgImage) {
						_assets.push(_bgImage);
					}
				});

				// after preloading assets
				let _letsGo = false;
				olivetree.preloadImg({
					assets: _assets,
					partial: function(assets) {
						// console.log('partial', assets);

						// // preload half of the assets
						// if(!_letsGo) {
						// 	_letsGo = true;

						// 	// stop the preloader
						// 	olivetree.preloader.stop(function() {
						// 		olivetree.ready();
						// 	});
						// }
					},
					after: function(assets) {
						// console.log('complete', assets);
						
						// preload half of the assets
						if(!_letsGo) {
							_letsGo = true;

							// stop the preloader
							olivetree.preloader.stop(function() {
								olivetree.ready();
							});
						}
					}
				});
			} else {
				olivetree.ready();
			}
		});

		// for aspect ratio
		$('.com_aspect-ratio').each(function() {
			olivetree.aspectRatio({target: this});
		});

		// for inview
		olivetree.inview.init();

		// for parallax items
		$('.com_anim-keyframe').each(function() {
			olivetree.animKeyframe({target: this});
		});

		// for link animate
		$('.link-animate, .links-animate a').each(function() {
			olivetree.linkAnimate({target: this});
		});

		// for carousels
		$('.com_owl-carousel').each(function() {
			const _carousel = this;
			let _responsive = true;

			const _options = {
				autoplay: false,
				loop: true,
				nav: true,
				dots: false,

				smartSpeed: 800,
				// autoplaySpeed: 800,
				navSpeed: 400,
				dotsSpeed: 400,

				navText: ['',''],

				items: 3,
				margin: 15,

				slideBy: 3,

				responsive: {
					0: {
						items: 1
					},
					640: {
						items: 2
					},
					768: {
						items: 3,
						dots: true,
						margin: 20
					},
					1080: {
						dots: true,
						margin: 38
					}
				}
			}

			olivetree.getAttr({
				target: _carousel,
				attr: 'carousel',
				after: function(attr) {
					// if responsive, desktop, mobile
					if(attr[0]) {
						_responsive = attr[0];
					}

					// custom slider
					if(attr[1]) {
						if(attr[1] == 'quotes') {
							_options.items = 1;
							_options.margin = 0;
							_options.responsive = {
								0: {
									dots: false,
									loop: true,
									margin: 15
								},
								768: {
									dots: true,
									loop: false,
									margin: 0
								}
							}
						}

						if(attr[1] == 'testimonials') {
							_options.items = 1;
							_options.margin = 0;
							_options.dots = false;
							_options.responsive = {
								0: {
									loop: true,
									margin: 15
								},
								768: {
									loop: false,
									margin: 0
								}
							}
						}
					}

					// if loop
					if(attr[2]) {
						if(attr[2] == 'false') {
							_options.loop = false;
						}
					}
				}
			});

			if(_responsive == 'responsive') {
				$(_carousel).addClass('owl-carousel');
				$(_carousel).owlCarousel(_options);
			}
		});

		// for popups
		$('[data-fancybox]').fancybox({
			animationEffect: 'zoom-in-out',
			transitionEffect: 'slide',
			slideShow: false,
			infobar: false,
			btnTpl: {
				smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}">' + '<i class="fal fa-times"></i>' +"</button>"
			},
		});

		// for tab slider
		$('.com_tab-slider').each(function() {
			new olivetree.tabSlider({target: this});
		});

		// for accordion
		$('.com_accordion').each(function() {
			var _closeOthers = this.getAttribute('close-others'),
			_openFirst = this.getAttribute('open-first');

			new olivetree.accordion({
				target: this,
				closeOthers: _closeOthers,
				openFirst: _openFirst
			});
		});

		// for google maps
		if(document.querySelector('.com_olive-tree-map')) {
			var _script = document.createElement('script');
			_script.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key='+ olivetreeMap.key +'&callback=olivetreeMap.init');
			document.querySelector('body').appendChild(_script);
		}

		// for select box
		$('.com_select-box').each(function() {
			olivetree.selectBox({target: this});
		});

		$('[data-item-hover]').each(function() {
			const _target = document.querySelector('.' + this.dataset.itemHover);

			if(_target) {
				$(this).mouseenter(function() {
					$(_target).addClass('on-hover');
				}).mouseleave(function() {
					$(_target).removeClass('on-hover');
				});
			}
		});

		// for search
		new olivetree.searchInput({target: document.querySelector('.com_search-input')});
	},
	ready: function() {
		// recalculate the spaces
		olivetree.dispatchEvent(window, 'resize');

		// show the contents
		olivetree.inview.run();

		// for earth animation
		new olivetree.earth({target: document.querySelector('.ho-co-earth')});

		// for parallax items
		$('.com_parallax-item').each(function() {
			new olivetree.parallax({target: this});
		});

		if(window.location.hash) {
			const _hash = ((window.location.hash).split('?'))[0];
			const _target = document.querySelector(_hash);

			if(_target) {
				// for auto scroll
				if($(_target).offset().top != 0) {
					setTimeout(function() {
						const _targetOffset = olivetree.st() - $(_target).offset().top;

						if(!(_targetOffset <= window.innerHeight * 0.3 && _targetOffset >= 0)) {
							olivetree.autoScroll(_target);
						}
					}, 300);
				}

				// for auto popup
				const _popupBtn = document.querySelector('a[href="'+ _hash +'"]');

				if(_popupBtn) {
					setTimeout(function() {
						_popupBtn.click();
					}, 300);
				}
			}
		}
	},
	width: function() {
		return window.innerWidth;
	},
	height: function() {
		return window.innerHeight;
	},
	header: {
		target: document.querySelector('.header-content'),
		init: function() {
			olivetree.header.main = olivetree.header.target.querySelector('.header-main');
			olivetree.header.top = olivetree.header.target.querySelector('.header-top');

			olivetree.scrollFn(function() {
				olivetree.header.scrolling();
			});

			olivetree.resizeFn(function() {
				olivetree.header.resize();
			});

			// for menu
			olivetree.header.menu.init();
		},
		lastScrollTop: 0,
		scrollable: true,
		scrolling: function() {
			if(olivetree.header.scrollable) {
				const _st = olivetree.st();
				const _range = _st >= olivetree.header.top.offsetHeight;

				if(_range) {
					$(olivetree.header.target).addClass('header-down');
				} else {
					$(olivetree.header.target).removeClass('header-down');
				}

				if(_st > olivetree.header.lastScrollTop) {
					// scrolling down
					if(_st >= olivetree.header.top.offsetHeight) {
						if($(olivetree.header.target).hasClass('header-absolute')) {
							TweenMax.set(olivetree.header.top, {y: -(olivetree.header.top.offsetHeight)});
						} else {
							$(olivetree.header.target).addClass('header-hide');
							TweenMax.to(olivetree.header.top, 0.3, {y: -(olivetree.header.top.offsetHeight)});
						}

						$(olivetree.header.target).removeClass('header-absolute');
					} else {
						$(olivetree.header.target).addClass('header-absolute');
					}
				} else {
					// scrolling up
					$(olivetree.header.target).removeClass('header-hide');
					TweenMax.to(olivetree.header.top, 0.3, {y: 0});
				}

				olivetree.header.lastScrollTop = _st <= 0 ? 0 : _st;
			}
		},
		resize: function() {
			$('.com_olive-tree-bottom').css({paddingTop: olivetree.header.target.offsetHeight});
		},
		menu: {
			target: '',
			cover: '',
			bar: '',
			init: function() {
				olivetree.header.menu.target = document.querySelector('.header-menu');
				olivetree.header.menu.cover = document.querySelector('.header-menu-cover');

				olivetree.header.menu.bar = $('.menu-bar')

				const _menu = olivetree.header.menu.target.querySelector('.menu');
				olivetree.listHover({target: _menu, items: _menu.querySelectorAll('.menu-set')});

				olivetree.header.menu.bar.click(function(e) {
					e.preventDefault();

					if($(this).hasClass('active')) {
						olivetree.header.menu.hide();
					} else {
						olivetree.header.menu.show();
					}
				});

				$(olivetree.header.menu.cover).click(function() {
					olivetree.header.menu.hide();
				});

				// everytime clicks on menu, hide the slide menu on mobile
				$('a', _menu).click(function(e) {
					if(window.innerWidth <= 640) {
						const _hash = this.getAttribute('href').split('#');

						if(_hash[1]) {
							const _content = document.querySelector('#' + _hash[1]);

							if(_content) {
								olivetree.header.menu.hide();
							}
						}
					}
				});

				// for menu-subs
				$('li', _menu).each(function() {
					const _ul = this.querySelector('ul');

					if(_ul) {
						const _arrow = document.createElement('div');
						$(_arrow).addClass('menu-sub-arrow').append('<i class="fa fa-angle-down"></i>');
						this.insertBefore(_arrow, _ul);

						$(this).addClass('menu-sub');
					}
				});

				$('.menu-sub-arrow', _menu).each(function() {
					const _parent = $(this).closest('.menu-sub');
					const _ul = $('ul', _parent);

					$(this).click(function() {
						if(_parent.hasClass('selected')) {
							$(_parent).removeClass('selected');
							$(_ul).slideUp(300);
						} else {
							const _curActive = _menu.querySelector('li.selected');
							if(_curActive) {
								$(_curActive).removeClass('selected');
								$('ul', _curActive).slideUp(300);
							}

							$(_parent).addClass('selected');
							$(_ul).slideDown(300);
						}

						olivetree.dispatchEvent(_menu, 'change');
					});
				});

				olivetree.resizeFn(function() {
					olivetree.header.menu.resize();
				});
			},
			show: function() {
				olivetree.header.scrollable = false;


				$('body').addClass('header-menu-active');

				olivetree.header.menu.bar.addClass('active');

				$(olivetree.header.menu.cover).show();
				TweenMax.fromTo(olivetree.header.menu.cover, 0.3, {opacity: 0}, {opacity: 1});

				$(olivetree.header.menu.target).show();
				TweenMax.fromTo(olivetree.header.menu.target, 1, {x: olivetree.header.menu.target.offsetWidth}, {x: 0, ease: Power4.easeOut});
				// ease: Power4.easeOut

				olivetree.header.menu.resize();
			},
			hide: function() {
				olivetree.header.scrollable = true;

				$('body').removeClass('header-menu-active');

				olivetree.header.menu.bar.removeClass('active');

				TweenMax.to(olivetree.header.menu.cover, 0.3, {opacity: 0, onComplete: function() {
					$(olivetree.header.menu.cover).hide();
				}});

				TweenMax.to(olivetree.header.menu.target, 1, {x: olivetree.header.menu.target.offsetWidth, ease: Power4.easeOut, onComplete: function() {
					$(olivetree.header.menu.target).hide();
				}});

				olivetree.header.menu.resize();
			},
			resize: function() {
				if($('body').hasClass('header-menu-active')) {
					const _headerTopHeight = olivetree.header.top.offsetHeight;

					$('.header-menu-main', olivetree.header.menu.target).css({top: _headerTopHeight, minHeight: olivetree.height() - _headerTopHeight});
				}
			}
		}
	},
	footer: {
		target: document.querySelector('footer'),
		height: function() {
			return olivetree.footer.target.offsetHeight;
		}
	},
	resize: function() {
		// sticky footer
		$(olivetree.footer.target).css({marginTop: -(olivetree.footer.height())});
		$('#main-wrapper').css({paddingBottom: olivetree.footer.height()});

		// for equal height
		$('.group-height').each(function() {
			olivetree.equalize(this.querySelectorAll('.gh1'));
			olivetree.equalize(this.querySelectorAll('.gh2'));
			olivetree.equalize(this.querySelectorAll('.gh3'));
			olivetree.equalize(this.querySelectorAll('.gh4'));
			olivetree.equalize(this.querySelectorAll('.gh5'));
		});

		$('.row-group-height').each(function() {
			var _this = this,
			_parentWidth = _this.parentNode.offsetWidth,
			_item = _this.querySelectorAll('.rgh-item'),
			_itemWidth = _item[0].offsetWidth,
			_columns = Math.round(_parentWidth / _itemWidth),
			_counter = 0,
			_numbering = 0,
			_elements = ['.rgh1', '.rgh2', '.rgh3', '.rgh4', '.rgh5'];

			// assign numbers in each '.rgh-item' base on group row
			for (var i = 0; i < _item.length; i++) {
				var _thisItem = _item[i];

				if(_counter == _columns) {
					_counter = 1;
					_numbering++;
				} else {
					_counter++;
				}

				_thisItem.setAttribute('rgh-col', _numbering);
			}

			for (var i = 0; i < _numbering + 1; i++) {
				// target all the columns per row
				var _thisRow = _this.querySelectorAll('[rgh-col="'+ i +'"]');

				for (var j = 0; j < _thisRow.length; j++) {
					var _thisGroup = _thisRow[j];

					for (var k = 0; k < _elements.length; k++) {
						// target all the elements base on group of columns
						var _element = _thisGroup.querySelector(_elements[k]);

						// assign number to each element
						if(_element) {
							_element.setAttribute('rgh-item', i + '-' + k);
						}

						// equalize the height of eah item
						olivetree.equalize(_this.querySelectorAll('[rgh-item="'+ i + '-' + k +'"'));
					}
				}
			}
		});

		// for background dependencies
		$('[data-bg-dependent]').each(function() {
			const _this  = this;
			const _target = document.querySelector('.' + _this.dataset.bgDependent);

			if(_target) {
				$(_this).css({'--top': _target.offsetTop + 'px'});
			}
		});

		$('.com_pull-half').each(function() {
			let _half = (this.offsetHeight / 3) * 2 + 20;

			if(olivetree.width() <= 480) {
				_half = 0;
			}

			$(this).css({marginBottom: -_half});
			$(this).closest('.com_pull-parent').css({marginBottom: _half});
		});

		$('[data-banner-pull]').each(function() {
			const _this = this;
			const _dataPull = this.dataset.bannerPull;

			if(_dataPull) {
				const _offset = _this.offsetHeight * (parseFloat(_dataPull) / 100);

				$(_this).css({top: _offset * -1 });
				$(_this).closest('[data-banner-parent]').css({marginTop: _offset});
			}
		});
	},
	equalize: function(target) {
		for (let i = 0; i < target.length; i++) {
			target[i].style.minHeight = 0;
		}

		let _biggest = 0;
		for (let i = 0; i < target.length; i++ ){
			let element_height = target[i].offsetHeight;
			if(element_height > _biggest ) _biggest = element_height;
		}

		for (let i = 0; i < target.length; i++) {
			target[i].style.minHeight = _biggest + 'px';
		}
		
		return _biggest;
	},
	resizeFn: function(fnctn) {
		let _resizeTimer = '';

		fnctn('init');
		window.addEventListener('resize', function() {
			fnctn('resize');

			clearTimeout(_resizeTimer);
			_resizeTimer = setTimeout(function() {
				fnctn('after');
			}, 300);
		});

		$(document).ready(function() {
			fnctn('ready');
		});
	},
	scrollFn: function(fnctn) {
		fnctn();
		window.addEventListener('scroll', function(e) {
			fnctn(e);
		});
	},
	isMobile: function() {
		const isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			return isMobile.any();
		}
	},
	getUrlVars: function() {
		let vars = {};
		const parts = window.location.href.replace(/[?&]+([^=&]+)=([^&#]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	},
	st: function() { return window.pageYOffset || document.documentElement.scrollTop },
	getAttr: function(opt) {
		const _attr = eval('opt.target.dataset.' + opt.attr);

		if(_attr) {
			if(opt.after) {
				if(opt.array) {
					opt.after(JSON.parse(_attr));
				} else {
					opt.after(_attr.split(','));
				}
			}
		}
	},
	olivePreloader: function(opt) {
		const _preloader = this;

		_preloader.target = '';

		_preloader.init = function() {
			_preloader.target = opt.target;

			_preloader.init = null;
		}

		_preloader.stop = function(fnctn) {
			let _count = 0;

			const _oliveOne = _preloader.target.querySelector('.olive-one');
			const _style = window.getComputedStyle(_oliveOne, null);

			const _inteval = setInterval(function() {
				_count++;

				const _transform = _style.getPropertyValue('-webkit-transform') || _style.getPropertyValue('-moz-transform') || _style.getPropertyValue('-ms-transform') || _style.getPropertyValue('-o-transform') || _style.getPropertyValue('transform');

				let _values = _transform.split('(')[1];
				_values = _values.split(')')[0];
				_values = _values.split(',');

				if(parseFloat(_values[1].replace(' ', '')).toFixed(3) == '0.999' || _count == 250) {
					clearInterval(_inteval);
					_preloader.complete();

					if(fnctn) {
						fnctn();
					}
				}
			}, 10);
		}

		_preloader.complete = function() {
			$(_preloader.target).addClass('preloader-finish');

			TweenMax.to($('.com_preloader-main', _preloader.target), 0.5, {opacity: 0, delay: 0.5});

			TweenMax.to(_preloader.target, 1.5, {backgroundColor: 'rgba(255,255,255,0)', onComplete: function() {
				$(_preloader.target).remove();
			}});
		}

		if(opt.target) {
			_preloader.init();
		}
	},
	preloadImg: function(opt) {
		if(opt.assets.length != 0) {
			if(opt.before) {
				opt.before();
			}

			let _loaded = 0,
			_assets = {
				loaded: [],
				error: []
			}

			const _push = function(state, image) {
				_loaded++;

				if(state == 'load') {
					_assets.loaded.push(image);
				} else {
					_assets.error.push(image);
				}

				if(_loaded >= Math.round(opt.assets.length * 0.6)) {
					if(opt.partial) {
						opt.partial(_assets);
					}
				}

				if(_loaded >= opt.assets.length) {
					if(opt.after) {
						opt.after(_assets);
					}
				}
			}

			for (let i = 0; i < opt.assets.length; i++) {
				const _img = new Image();
				_img.src = opt.assets[i].getAttribute('src');

				_img.addEventListener('load', function() {
					_push('load', _img);
				});

				_img.addEventListener('error', function() {
					_push('error', _img);
				});
			} 
		} else {
			if(opt.after) {
				opt.after();
			}
		}
	},
	aspectRatio: function(opt) {
		const _ar = {
			target: '',
			base: {
				width: 0,
				height: 0,
				maxWidth: false,
				maxHeight: false,
				ratio: 0,
			},
			current: {
				width: 0,
				height: 0
			},
			init: function() {
				_ar.target = opt.target;

				olivetree.getAttr({
					target: _ar.target,
					attr: 'aspectRatio',
					after: function(attr) {
						_ar.base.width = parseInt(attr[0]);
						_ar.base.height = parseInt(attr[1]);

						if(attr[2] == 'maxWidth') {
							_ar.base.maxWidth = true;
						}

						if(attr[2] == 'maxHeight') {
							_ar.base.maxHeight = true;
						}

						if(attr[3]) {
							_ar.base.ratio = parseInt(attr[3].replace('%', '')) / 100;
						}
					}
				});

				olivetree.resizeFn(function() {
					_ar.update();
				});
			},
			update: function() {
				const _parentWidth = _ar.target.parentNode.offsetWidth,
				_parentHeight = _ar.target.parentNode.offsetHeight;

				if(_parentWidth <= 0) {
					// if parents width is 0, use base width
					_ar.current.width = _ar.base.width;
				} else {
					// use parents width
					_ar.current.width = _parentWidth;
				}

				// if need to control maxWidth
				if(_ar.base.maxWidth) {
					if(_ar.current.width >= _ar.base.width) {
						_ar.current.width = _ar.base.width;
					}
				}

				if(_ar.base.maxHeight) {
					// prioritize the base height
					_ar.current.height = _ar.target.parentNode.offsetHeight * _ar.base.ratio;

					_ar.target.style.width = ( _ar.base.width * (_ar.current.height / _ar.base.height) ) + 'px';
					_ar.target.style.height = _ar.current.height + 'px';
				} else {
					// usual resize
					_ar.target.style.width = _ar.current.width + 'px';
					_ar.target.style.height =  ( _ar.base.height * (_ar.current.width / _ar.base.width) ) + 'px';
				}
			}
		}
		_ar.init();
	},
	dispatchEvent: function(elem, eventName) {
		let event;
		if (typeof(Event) === 'function') {
			event = new Event(eventName);
		}
		else {
			event = document.createEvent('Event');
			event.initEvent(eventName, true, true);
		}
		elem.dispatchEvent(event);
	},
	inview: {
		init: function() {
			!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){function i(){var b,c,d={height:f.innerHeight,width:f.innerWidth};return d.height||(b=e.compatMode,(b||!a.support.boxModel)&&(c="CSS1Compat"===b?g:e.body,d={height:c.clientHeight,width:c.clientWidth})),d}function j(){return{top:f.pageYOffset||g.scrollTop||e.body.scrollTop,left:f.pageXOffset||g.scrollLeft||e.body.scrollLeft}}function k(){if(b.length){var e=0,f=a.map(b,function(a){var b=a.data.selector,c=a.$element;return b?c.find(b):c});for(c=c||i(),d=d||j();e<b.length;e++)if(a.contains(g,f[e][0])){var h=a(f[e]),k={height:h[0].offsetHeight,width:h[0].offsetWidth},l=h.offset(),m=h.data("inview");if(!d||!c)return;l.top+k.height>d.top&&l.top<d.top+c.height&&l.left+k.width>d.left&&l.left<d.left+c.width?m||h.data("inview",!0).trigger("inview",[!0]):m&&h.data("inview",!1).trigger("inview",[!1])}}}var c,d,h,b=[],e=document,f=window,g=e.documentElement;a.event.special.inview={add:function(c){b.push({data:c,$element:a(this),element:this}),!h&&b.length&&(h=setInterval(k,250))},remove:function(a){for(var c=0;c<b.length;c++){var d=b[c];if(d.element===this&&d.data.guid===a.guid){b.splice(c,1);break}}b.length||(clearInterval(h),h=null)}},a(f).on("scroll resize scrollstop",function(){c=d=null}),!g.addEventListener&&g.attachEvent&&g.attachEvent("onfocusin",function(){d=null})});

			// animate containers that are parents/child
			$('.animate').each(function(index) {
				const _this = this;

				const _dataset = _this.dataset.animate;
				if(_dataset) {
					const _attr = _dataset.split(',');

					// if parent, put index number
					if(_attr[0] == 'parent') {
						$(_this).attr('animate-index', index);
					}

					// if child, group them to its parent
					if(_attr[0] == 'child') {
						$(_this).removeClass('animate');
						const _parentIndex = $(_this).parent().closest('.animate').attr('animate-index');

						if(_parentIndex) {
							$('> *', _this).addClass('animate-child' + _parentIndex);
						}
					}
				}
			});
		},
		run: function() {
			$('.animate').each(function() {
				olivetree.inview.animate({target: this, clear: true});
			});

			$('.inview').each(function() {
				$(this).one('inview', function(event, visible) {
					if(visible) {
						$(this).addClass('visible');
					}
				});
			});
		},
		animate: function(opt) {
			const _animate = {
				target: null,
				custom: null,
				mobileCustom: null,
				parent: false,
				delay: null,
				index: null,
				init: function() {
					_animate.target = opt.target;

					const _dataset = _animate.target.dataset.animate;
					if(_dataset) {
						const _attr = _dataset.split(',');

						// dont animate the parent
						if(_attr[0] == 'parent') {
							$(_animate.target).removeClass('animate');
							_animate.parent = true;

							// get the parent index
							_animate.index = parseInt(_animate.target.getAttribute('animate-index'));
						}

						// custom css animation
						if(_attr[1]) {
							if(_attr[1] != 'false') {
								_animate.custom = _attr[1];
							}
						}

						// animation delay
						if(_attr[2]) {
							_animate.delay = parseFloat(_attr[2]);
						}

						// if custom css animation for mobile
						if(_attr[3]) {
							if(_attr[3] != 'false') {
								_animate.mobileCustom = _attr[3];
							}
						}
					}

					$(_animate.target).one('inview', function(event, visible) {
						if(visible) {
							if(window.innerWidth <= 768) {
								_animate.custom = _animate.mobileCustom;
							}

							if(_animate.parent) {
								// for parent
								$('.animate-child' + _animate.index).each(function(index) {
									// animate all children at once with delays
									if(index != 0) {
										_animate.delay = _animate.delay + 0.1;
									}

									olivetree.inview.validate(this, _animate.delay, _animate.custom, opt.clear, _animate.index)
								});

								if(opt.clear) {
									$(_animate.target).removeAttr('animate-index');
								}
							} else {
								// for single
								olivetree.inview.validate(_animate.target, _animate.delay, _animate.custom, opt.clear);
							}
						}
					});
				}
			}
			_animate.init();
		},
		validate: function(target, delay, custom, clear, index) {
			// add delay
			if(delay) {
				$(target).css('animation-delay', delay + 's');
			}

			if(custom) {
				// add custom animation
				$(target).addClass('anim-custom ' + custom);
			} else {
				// common animation
				$(target).addClass('anim-content');
			}

			// show the animation
			$(target).addClass('visible');

			// remove all animate attributes
			if(clear) {
				setTimeout(function() {
					$(target).removeClass('animate anim-content anim-custom visible');

					if(custom) {
						$(target).removeClass(custom);
					}

					if(index != null) {
						$(target).removeClass('animate-child' + index);
					}

					$(target).css('animation-delay', '');
				}, (0.8 + delay) * 1000);
			}
		}
	},
	animKeyframe: function(opt) {
		const _anim = {
			target: '',
			frames: '',
			axis: 0,
			defOffset: 0.5,
			init: function() {
				_anim.target = opt.target;

				olivetree.getAttr({
					target: _anim.target,
					attr: 'animKeyframe',
					array: true,
					after: function(attr) {
						if(attr) {
							_anim.frames = attr;
						}
					}
				});

				olivetree.resizeFn(function() {
					_anim.axis = 0;

					for (let i = 0; i < _anim.frames.length; i++) {
						const _thisFrame = _anim.frames[i],
						_thisWidth = parseInt(Object.keys(_thisFrame)[0]),
						_thisAxis = parseInt(Object.values(_thisFrame)[0]);

						if(olivetree.width() < _thisWidth) {
							_anim.axis = _thisAxis;
						}
					}

					_anim.update();
				}, true);

				olivetree.scrollFn(function() {
					_anim.update();
				});

				_anim.update();
			},
			update: function() {
				const _offsetTop = $(_anim.target).offset().top;
				const _top = _offsetTop - olivetree.st();

				TweenMax.set(_anim.target, {'--offset': 0});

				if(_top <= olivetree.height() *_anim.defOffset) {
					// above screen
					const _scrolled = _top - olivetree.height() *_anim.defOffset;
					const _range = olivetree.height() *_anim.defOffset;
					let _computation = (_scrolled * -1) / _range;

					// if(_computation >= 1) {
					// 	_computation = 1;
					// }

					TweenMax.set(_anim.target, {'--offset': -(_anim.axis * _computation) + 'px'});
				} else {
					// below screen
					const _scrolled = _top - (olivetree.height() *_anim.defOffset);
					const _range = olivetree.height() *_anim.defOffset;
					let _computation = _scrolled / _range;

					if(_computation >= 1) {
						_computation = 1;
					}

					TweenMax.set(_anim.target, {'--offset': (_anim.axis * _computation) + 'px'});
				}
			}
		}
		_anim.init();
	},
	linkAnimate: function(opt) {
		const _link = {
			target: '',
			color: '',
			custom: '',
			class: '',
			href: '',
			after: '',
			init: function() {
				_link.target = opt.target;

				// get the attributes
				olivetree.getAttr({
					target: _link.target,
					attr: 'linkAnimate',
					after: function(attr) {
						if(attr[0] != 'false') {
							_link.color = attr[0];
						}

						if(attr[1] != 'false') {
							_link.custom = attr[1];
						}

						if(attr[2] != 'false') {
							_link.class = attr[2];
						}
					}
				});

				// get the link
				const _href = _link.target.getAttribute('href');
				if(_href && _href != '#') {
					_link.href = _href;
				}
				if(_link.target.getAttribute('target') == '_blank') {
					_link.href = null;
				}

				$(_link.target).click(function(e) {
					let _dontAnimate = false;

					if($(this).attr('dont-animate') == 'true') {
						e.preventDefault();

						_dontAnimate = true;
					}

					// if link has ID and its in the page, auto scroll and dont animate
					const _hash = _link.href.split('#');
					if(_hash[1]) {
						const _content = document.querySelector('#' + _hash[1]);

						if(_content) {
							e.preventDefault();
							_dontAnimate = true;

							var _offset = false;
							var _speed = false;

							var _autoScroll = _link.target.dataset.autoScroll;
							if(_autoScroll) {
								_autoScroll = _autoScroll.split(',');

								_offset = parseInt(_autoScroll[0]);
								_speed = parseInt(_autoScroll[1]);
							}

							olivetree.autoScroll(_content, _offset, _speed);
						}
					}

					// if(_link.href && !_dontAnimate) {
					// 	e.preventDefault();

					// 	if(_link.custom) {
					// 		if(_link.after) {
					// 			_link.after();
					// 		}
					// 	} else {
					// 		// create link cover
					// 		let _cover = document.querySelector('.com_link-cover')
					// 		if(!_cover) {
					// 			_cover = document.createElement('div');
					// 			_cover.classList.add('com_link-cover');
					// 		}
							
					// 		if(_link.color) {
					// 			_cover.style.backgroundColor = _link.color;
					// 		}
					// 		if(_link.class) {
					// 			_cover.classList.add(_link.class);
					// 		}
					// 		document.body.appendChild(_cover);

					// 		// set the width, height of cover
					// 		let _size = olivetree.height() * 2.3;
					// 		if(olivetree.width() >= olivetree.height()) {
					// 			_size = olivetree.width() * 2.3
					// 		}

					// 		// animate the cover
					// 		TweenMax.set(_cover, {top: e.clientY, left: e.clientX});
					// 		TweenMax.to(_cover, 1, {width: _size, height: _size, delay: 0.2, onComplete: function() {
					// 			_link.open();

					// 			TweenMax.to(_cover, 0.5, {opacity: 0, delay: 5, onComplete: function() {
					// 				$(_cover).remove();
					// 			}});
					// 		}});
					// 	}
					// }
				});
			},
			open: function() {
				window.open(_link.href, '_self');
			}
		}
		_link.init();
	},
	enquire: function(opt) {
		const _enquire = {
			status: 'once',
			desktop: opt.desktop,
			mobile: opt.mobile,
			counterDesktop: 0,
			counterMobile: 0,
			width: 0,
			between: 810,
			init: function() {
				if(opt.between) {
					_enquire.between = opt.between;
				}

				if(opt.status) {
					_enquire.status = opt.status;
				}

				_enquire.update();
				window.addEventListener('resize', function() {
					_enquire.update();
				});
			},
			update: function() {
				_enquire.width = $(window).outerWidth();

				if(_enquire.status == 'always') {
					// update every resize
					if(_enquire.width > _enquire.between) {
						_enquire.desktop();
					} else {
						_enquire.mobile();
					}
				} else {
					// if detected desktop or mobile then update once
					if(_enquire.width > _enquire.between) {
						_enquire.counterMobile = 0;

						// if desktop
						if(_enquire.counterDesktop == 0) {
							_enquire.counterDesktop = 1;
							_enquire.desktop();
						}
					} else {
						_enquire.counterDesktop = 0;

						// if mobile
						if(_enquire.counterMobile == 0) {
							_enquire.counterMobile = 1;
							_enquire.mobile();
						}
					}
				}
			}
		}
		_enquire.init();
	},
	earth: function(opt) {
		const _earth = this;

		_earth.target = '';

		_earth.init = function() {
			_earth.target = opt.target;

			// for snow animation
			// new olivetree.earthSnow({
			// 	target: _earth.target.querySelector('.com_earth-snow'),
			// 	count: 35
			// });

			// for speech bubbles
			new olivetree.earthBubbles({
				target: _earth.target.querySelector('.ho-co-speech-bubbles'),
				autoBubble: true
			});

			_earth.init = null;
		}

		if(opt.target) {
			_earth.init();
		}
	},
	earthSnow: function(opt) {
		const _snow = this;

		_snow.target = '';
		_snow.count = 50;
		_snow.styles = 8;

		_snow.width = 0;
		_snow.height = 0;

		_snow.init = function() {
			_snow.target = opt.target;

			if(opt.count) {
				_snow.count = parseFloat(opt.count);
			}

			olivetree.resizeFn(function() {
				_snow.width = _snow.target.offsetWidth;
				_snow.height = _snow.target.offsetHeight;
			});

			// create snows
			for (let i = 0; i < _snow.count; i++) {
				const _start = Math.floor( (Math.random() * 500) );
				let _count = 0;

				const _interval = setInterval(function() {
					_count++;

					if(_count >= _start) {
						clearInterval(_interval);
						_snow.create();
					}
				}, 10);
			}

			_snow.init = null;
		}

		_snow.create = function(state) {
			const _item = document.createElement('div');

			// set size of this snow
			$(_item).addClass('co-ea-snow');

			const _style = Math.floor( (Math.random() * _snow.styles) + 1);
			$(_item).addClass('snow-' +  _style);

			// show this snow
			_snow.target.appendChild(_item);

			// set left widthin width of parent
			TweenMax.set(_item, {x: Math.floor( (Math.random() * _snow.width) ) });

			// set speed
			let _speed = Math.floor((Math.random() * 5) + 5);

			// set top start
			let _topStart = 0;

			// set top end
			let _topEnd = Math.floor( (Math.random() * (_snow.height / 2)) + (_snow.height / 2) ) - _item.offsetHeight;

			// console.log('height', _snow.height, '_topStart', _topStart, '_topEnd', _topEnd, _topStart <= _topEnd );

			// animate fall
			TweenMax.fromTo(_item, 0.3, {opacity: 0}, {opacity: 1});
			TweenMax.fromTo(_item, _speed, {y: _topStart}, {y: _topEnd, ease: 'linear', onComplete: function() {
				TweenMax.to(_item, 0.3, {opacity: 0, onComplete: function() {
					$(_item).remove();
					_snow.create();
				}});
			}});
		}

		if(opt.target) {
			_snow.init();
		}

		// console.log(_snow);
	},
	earthBubbles: function(opt) {
		const _bubbles = this;

		_bubbles.target = '';
		_bubbles.onHover = false;

		_bubbles.automatic = false;
		_bubbles.autoBubbleCount = 0;

		_bubbles.init = function() {
			_bubbles.target = opt.target;

			if(opt.autoBubble) {
				_bubbles.automatic = true;
			}

			_bubbles.items = _bubbles.target.querySelectorAll('.hc-sp-item');
			for (let i = 0; i < _bubbles.items.length; i++) {

				// upon bubble interaction
				$(_bubbles.items[i]).mouseenter(function() {
					if(!$(this).hasClass('active')) {
						_bubbles.show(this);
					}

					_bubbles.onHover = true;
					_bubbles.autoBubbleCount = 0;
				}).mouseleave(function() {
					_bubbles.hide(this);

					_bubbles.onHover = false;
				});
			}

			// autoplay showing of speech bubbles
			if(_bubbles.automatic) {
				_bubbles.autoBubble();
			}

			_bubbles.init = null;
		}

		_bubbles.show = function(target) {
			// first; if theres active bubble, hide it first
			const _currentActive = _bubbles.target.querySelector('.hc-sp-item.active');
			if(_currentActive) {
				_bubbles.hide(_currentActive);
			}

			// show selected bubble
			$(target).addClass('active');

			const _thisBuble = target.querySelector('.hc-sp-bubble');

			TweenMax.to(_thisBuble, 0.3, {opacity: 1});
			TweenMax.fromTo(_thisBuble, 1, {scale: 0.8}, {scale: 1, ease: Elastic.easeOut});
		}

		_bubbles.hide = function(target) {
			// hide the bubble
			$(target).removeClass('active');

			const _thisBuble = target.querySelector('.hc-sp-bubble');

			TweenMax.to(_thisBuble, 0.3, {opacity: 0});
			TweenMax.to(_thisBuble, 1, {scale: 0.8, ease: Elastic.easeOut});
		}

		_bubbles.autoBubble = function() {
			// _bubbles.autoBubbleCount = 20;

			// auto select bubble counter
			let _limit = 40;
			let _currentNumber = 0;

			const _interval = setInterval(function() {
				_bubbles.autoBubbleCount++;

				if(_bubbles.autoBubbleCount >= _limit) {
					_bubbles.autoBubbleCount = 0;

					if(!_bubbles.onHover) {
						const _currentActive = _bubbles.target.querySelector('.hc-sp-item.active');

						if(_currentActive) {
							// hide the automatically selected bubble
							_bubbles.hide(_currentActive);
						} else {
							// show the automatically selected bubble

							let _number = 0;
							const _randomize = function() {
								_number = Math.floor( Math.random() * _bubbles.items.length );

								// if same bubble as prev, select random again
								if(_number == _currentNumber) {
									_randomize();
								}
							}
							_randomize();

							_currentNumber = _number;
							_bubbles.show(_bubbles.items[_number]);
						}
					}
				}
			}, 100);
		}

		if(opt.target) {
			_bubbles.init();
		}

		// console.log(_bubbles);
	},
	parallax: function(opt) {
		const _parallax = this;

		_parallax.target = '';
		_parallax.coords = {
			x: 0,
			y: 0
		}
		_parallax.maxRange = {
			x: 0,
			y: 0
		}

		_parallax.init = function() {
			_parallax.target = opt.target;

			olivetree.getAttr({
				target: _parallax.target,
				attr: 'parallax',
				after: function(attr) {
					if(attr[0]) {
						_parallax.coords.x = parseInt(attr[0]);
					}

					if(attr[1]) {
						_parallax.coords.y = parseInt(attr[1]);
					} else {
						_parallax.coords.y = parseInt(attr[0]);
					}
				}
			});

			olivetree.resizeFn(function() {
				_parallax.maxRange.x = olivetree.width() / 2;
				_parallax.maxRange.y = olivetree.height() / 2;
			});

			$('body').on('mousemove', function(e) {
				// get mouse position
				let _coords = {
					x: e.clientX,
					y: e.clientY
				}

				// get center position of the item
				let _itemCoords = {
					x: _coords.x - ( $(_parallax.target).parent().offset().left + ($(_parallax.target).parent().outerWidth() / 2) ),
					y: _coords.y - ( ($(_parallax.target).parent().offset().top - olivetree.st()) + ($(_parallax.target).parent().outerHeight() / 2) )
				}

				// set max range
				if(_itemCoords.x >= _parallax.maxRange.x) {
					_itemCoords.x = _parallax.maxRange.x;
				}

				if(_itemCoords.y >= _parallax.maxRange.y) {
					_itemCoords.y = _parallax.maxRange.y;
				}

				let _top = _parallax.coords.y * (_itemCoords.y / _parallax.maxRange.y);
				let _left = _parallax.coords.x * (_itemCoords.x / _parallax.maxRange.x);

				TweenMax.to(_parallax.target, 0.5, {'--y': _top + 'px', '--x': _left + 'px'});
			});

			_parallax.init = null;
		}

		if(opt.target) {
			_parallax.init();
		}

		// console.log(_parallax);
	},
	listHover: function(opt) {
		const _hover = {
			target: '',
			items: '',
			indicator: '',
			currentItem: '',
			init: function() {
				_hover.target = opt.target;
				_hover.items = opt.items;

				_hover.indicator = document.createElement('div');
				_hover.indicator.classList.add('com_list-hover-indicator');
				_hover.target.appendChild(_hover.indicator);
				TweenMax.set(_hover.indicator, {opacity: 0});

				$(_hover.items).each(function() {
					$(this).mouseenter(function() {
						_hover.update(this);
					});
				});

				$(_hover.target).mouseleave(function() {
					_hover.hide();
				});

				let _changeTimer = '';
				_hover.target.addEventListener('change', function() {
					_hover.update(_hover.currentItem);

					clearTimeout(_changeTimer);
					_changeTimer = setTimeout(function() {
						_hover.update(_hover.currentItem);
					}, 300);
				});
			},
			update: function(item) {
				_hover.currentItem = item;

				const _top = $(item).offset().top - $(_hover.target).offset().top;
				const _left = $(item).offset().left - $(_hover.target).offset().left;

				if(!($(_hover.indicator).hasClass('active'))) {
					TweenMax.set(_hover.indicator, {width: item.offsetWidth, height: item.offsetHeight, top: _top, left: _left});
					TweenMax.to(_hover.indicator, 0.3, {opacity: 1});

					$(_hover.indicator).addClass('active');
				} else {
					TweenMax.to(_hover.indicator, 0.2, {width: item.offsetWidth, height: item.offsetHeight, top: _top, left: _left});
				}
			},
			hide: function() {
				TweenMax.to(_hover.indicator, 0.5, {opacity: 0});

				$(_hover.indicator).removeClass('active');
			}
		}
		_hover.init();
	},
	tabSlider: function(opt) {
		const _slider = this;

		_slider.target = '';
		_slider.nav = '';
		_slider.items = '';

		_slider.flexslider = '';
		_slider.currentSlide = 0;

		_slider.accordion = '';

		_slider.init = function() {
			_slider.target = opt.target;
			_slider.main = _slider.target.querySelector('.com_tab-slider-main');
			_slider.nav = _slider.target.querySelector('.com_tab-slider-nav');

			_slider.items = _slider.target.querySelectorAll('.com_tab-slider-items');

			for (let i = 0; i < _slider.items.length; i++) {
				$(_slider.items[i]).attr('item-index', i);
			}

			const _navParent = _slider.nav.parentNode;

			olivetree.enquire({
				between: 767,
				desktop: function() {
					// flexslider for desktop

					// if accordion for mobile is active, destroy it
					if(_slider.accordion) {
						_slider.accordion.destroy();

						// remove every prints from accordion
						for (let i = 0; i < _slider.items.length; i++) {
							const _thisItem = _slider.items[i];

							$(_thisItem).removeClass('com_accordion-set');
							$('.com_tab-slider-title', _thisItem).removeClass('com_accordion-title');
							$('.com_tab-slider-content', _thisItem).removeClass('com_accordion-content');
						}

						_slider.accordion = null;
					}

					// ready the slider
					_slider.flexslider = document.createElement('div');
					$(_slider.flexslider).addClass('flexslider');

					const _slides = document.createElement('ul');
					$(_slides).addClass('slides');
					$(_slider.flexslider).append(_slides);

					_slider.main.appendChild(_slider.flexslider);

					for (let i = 0; i < _slider.items.length; i++) {
						const _li = document.createElement('li');

						$(_li).append(_slider.items[i].querySelector('.com_tab-slider-content'));
						$(_slides).append(_li);
					}

					// init the slider
					$(_slider.flexslider).flexslider({
						slideshow: false,
						animation: 'fade',
						controlNav: false,
						directionNav: false,
						animationSpeed: 300,
						startAt: _slider.currentSlide,
						start: function(slider) {
							$(_slider.items[slider.currentSlide]).addClass('active');

							$(_slider.items).click(function(e, i) {
								if(_slider.flexslider) {
									e.preventDefault();
									const _index = parseFloat(this.getAttribute('item-index'));

									if(_index != _slider.currentSlide) {
										$(_slider.flexslider).flexslider(_index);
									}
								}
							});
						},
						before: function(slider) {
							const _currentSlide = slider.animatingTo;

							$(_slider.items).removeClass('active');
							$(_slider.items[_currentSlide]).addClass('active');

							_slider.currentSlide = _currentSlide;
						}
					});

					_navParent.appendChild(_slider.nav);
				},
				mobile: function() {
					// accordion for mobile

					// if flexslider for desktop is active, destroy it
					if(_slider.flexslider) {
						for (let i = 0; i < _slider.items.length; i++) {
							const _content = _slider.target.querySelectorAll('.com_tab-slider-content')[i];

							$(_slider.items[i]).removeClass('active');
							$(_slider.items[i]).append(_content);
						}

						$(_slider.flexslider).remove();
						_slider.flexslider = null;
					}

					// ready the accordion
					for (let i = 0; i < _slider.items.length; i++) {
						const _thisItem = _slider.items[i];

						$(_thisItem).addClass('com_accordion-set');
						$('.com_tab-slider-title', _thisItem).addClass('com_accordion-title');
						$('.com_tab-slider-content', _thisItem).addClass('com_accordion-content');
					}

					// which item should be selected upon plugin init
					$(_slider.items[_slider.currentSlide]).addClass('selected');

					// init the accordion
					_slider.accordion = new olivetree.accordion({
						target: _slider.target,
						after: function(accordion) {
							_slider.currentSlide = accordion.current;
						}
					});

					_slider.main.appendChild(_slider.nav);
				}
			})

			_slider.init = null;
		}

		if(opt.target) {
			_slider.init();
		}

		// console.log(_slider);
	},
	accordion: function(opt) {
		const _accordion = this;

		_accordion.target = '';
		_accordion.closeOthers = true;
		_accordion.openFirst = false;
		_accordion.list = '';

		_accordion.current = 0;

		_accordion.init = function() {
			_accordion.target = opt.target;

			if(opt.closeOthers) {
				if(opt.closeOthers == 'true') {
					_accordion.closeOthers = true;
				} else {
					_accordion.closeOthers = false;
				}
			}

			if(opt.openFirst) {
				if(opt.openFirst == 'true') {
					_accordion.openFirst = true;
				} else {
					_accordion.openFirst = false;
				}
			}

			_accordion.list = _accordion.target.querySelectorAll('.com_accordion-set');
			for (var i = 0; i < _accordion.list.length; i++) {
				var _this = _accordion.list[i],
				_title = _this.querySelector('.com_accordion-title');

				_this.setAttribute('set-index', i);

				_title.addEventListener('click', function(e) {
					e.preventDefault();

					if(this.parentNode.classList.contains('selected')) {
						_accordion.close(this.parentNode);
					} else {
						_accordion.open(this.parentNode, true, parseFloat(this.parentNode.getAttribute('set-index')));
					}
				});
			}

			// open the custom selected set
			var _activeSet = _accordion.target.querySelectorAll('.com_accordion-set.selected');
			if(_activeSet) {
				for (var i = 0; i < _activeSet.length; i++) {
					_accordion.open(_activeSet[i], 'no-scroll');
				}
			}

			// open the first set
			if(!_activeSet[0]) {
				if(_accordion.openFirst) {
					_accordion.open(_accordion.list[0], 'no-scroll');
				}
			}
		}

		_accordion.open = function(target, state, index) {
			if(_accordion.target) {
				if(_accordion.closeOthers) {
					var _activeSet = _accordion.target.querySelectorAll('.com_accordion-set.selected');

					if(_activeSet) {
						for (var i = 0; i < _activeSet.length; i++) {
							_accordion.close(_activeSet[i]);
						}
					}
				}

				target.classList.add('selected');

				var _content = target.querySelector('.com_accordion-content');
				$(_content).slideDown(function() {
					if(state != 'no-scroll') {
						var st = window.pageYOffset || document.documentElement.scrollTop;
						
						if($(_content).offset().top - olivetree.header.top.offsetHeight <= st) {
							$('body, html').animate({scrollTop:$(target).offset().top -( olivetree.header.top.offsetHeight + 15 ) }, 700);
						}
					}
				});

				_accordion.current = index;

				if(opt.after) {
					opt.after(_accordion);
				}

				olivetree.dispatchEvent(window, 'resize');
			}
		}

		_accordion.close = function(target) {
			if(_accordion.target) {
				target.classList.remove('selected');

				var _content = target.querySelector('.com_accordion-content');
				$(_content).slideUp();
			}
		}

		_accordion.destroy = function() {
			_accordion.target = null;

			for (let i = 0; i < _accordion.list.length; i++) {
				const _this = _accordion.list[i];

				$(_this).removeClass('selected');
				$(_this).removeAttr('set-index');

				$('.com_accordion-content', _this).css({display: ''});
			}
		}

		if(opt.target) {
			_accordion.init();
		}
	},
	autoScroll: function(target, offset, speed) {
		let _offset = 0;

		// if going to scroll up, get header height as offset
		if($(target).offset().top <= olivetree.st()) {
			_offset = $('.header-top').outerHeight();
		}

		if(parseInt(offset)) _offset = offset;

		let _speed = 700;
		if($(target).offset().top - olivetree.st() >= 2000) {
			_speed = 1000;
		}
		if(parseInt(speed)) _speed = speed;

		$('body, html').animate({scrollTop: $(target).offset().top -(_offset) }, _speed);
	},
	selectBox: function(opt) {
		var _select = {
			target: '',
			select: '',
			title: '',
			list: '',
			options: '',
			active: 0,	
			init: function() {
				_select.target = opt.target;

				// hide the <select>
				_select.select = _select.target.querySelector('select');
				_select.select.style.display = 'none';
				_select.options = _select.select.querySelectorAll('option');

				// create title
				_select.title = document.createElement('div');
				_select.title.classList.add('com_select-box-title');
				_select.target.appendChild(_select.title);

				// create list
				_select.list = document.createElement('div');
				_select.list.classList.add('com_select-box-list');

				var _ul = document.createElement('ul'),
				_firstSelected = 0;
				for (var i = 0; i < _select.options.length; i++) {
					var _thisOption = _select.options[i],
					_li = document.createElement('li'),
					_selected = _thisOption.getAttribute('selected');

					_li.innerHTML = _thisOption.innerHTML;

					_ul.appendChild(_li);

					// for li clicks
					_select.interaction(_li, i);

					// get the selected option
					if(_selected != null) {
						_firstSelected = i;
					}
				}
				_select.list.appendChild(_ul);
				_select.target.appendChild(_select.list);

				// for title click
				_select.title.addEventListener('click', function(e) {
					e.preventDefault();

					if(_select.options.length != 1) {
						if(_select.target.classList.contains('com_select-box-active')) {
							_select.hide();
						} else {
							_select.show();
						}
					}
				});

				// when clicking outside the select
				window.addEventListener('click', function(e){   
					if (!(_select.target.contains(e.target))){
						_select.hide();
					}
				});

				// <select> event
				_select.select.addEventListener('change', function() {
					_select.update(_select.select.selectedIndex, 'self');
				});

				// select the first entry
				_select.update(_firstSelected);

				// _select.show();
			},
			update: function(number, state) {
				_select.active = number;

				// active the current list
				var _list = _select.list.querySelectorAll('li');
				for (var i = 0; i < _list.length; i++) {
					if (i == number) {
						_list[i].classList.add('active');
					} else {
						_list[i].classList.remove('active');
					}
				}

				// change the title text
				_select.title.innerHTML = _select.options[_select.active].innerHTML;

				// if value are default
				var _value = _select.options[_select.active].value; 
				if(_value == 'default') {
					_select.target.classList.add('com_select-box-default');
				} else {
					_select.target.classList.remove('com_select-box-default');
				}

				// trigger <select>
				if(state != 'self') {
					_select.select.selectedIndex = number;
					olivetree.dispatchEvent(_select.select, 'change');
				}
			},
			show: function() {
				_select.target.classList.add('com_select-box-active');
			},
			hide: function() {
				_select.target.classList.remove('com_select-box-active');
			},
			interaction: function(target, index) {
				target.addEventListener('click', function(e) {
					e.preventDefault();

					_select.hide();
					if(_select.active != index) {
						_select.update(index);
					}
				});
			}
		}
		_select.init();
	},
	searchInput: function(opt) {
		const _search = this;

		_search.target = '';
		_search.input = '';

		_search.init = function() {
			_search.target = opt.target;
			_search.input = _search.target.querySelector('input');

			$(_search.target).mouseenter(function() {
				_search.show();
			}).mouseleave(function() {
				_search.hide();
			});

			_search.input.addEventListener('focus', function() {
				$(_search.target).addClass('search-focus');
			});

			// when clicking outside the select
			window.addEventListener('click', function(e){   
				if (!(_search.target.contains(e.target))){
					$(_search.target).removeClass('search-focus');
					_search.hide();
				}
			});

			olivetree.resizeFn(function() {
				if($(_search.target).hasClass('search-active')) {
					if(window.innerWidth <= 768) {
						$(_search.input).css({width: $('body').outerWidth() - 30});
					}
				}
			});

			_search.init = null;
		}

		_search.show = function() {
			if(!($(_search.target).hasClass('search-active'))) {
				$(_search.target).addClass('search-active');

				$(_search.input).show();
				let _inputWidth = parseFloat( $(_search.input).css('max-width') );

				if(window.innerWidth <= 768) {
					_inputWidth = $('body').outerWidth() - 30;
				}

				TweenMax.fromTo(_search.input, 0.3, {opacity: 0, width: _search.target.offsetWidth}, {opacity: 1, width: _inputWidth, onComplete: function() {
					if(olivetree.isMobile()) {
						_search.input.focus();
					}
				}});
			}
		}

		_search.hide = function() {
			if(!($(_search.target).hasClass('search-focus'))) {
				$(_search.target).removeClass('search-active');
				TweenMax.to(_search.input, 0.3, {opacity: 0, width: _search.target.offsetWidth, onComplete: function() {
					_search.input.blur();
					$(_search.input).hide();
				}});
			}
		}

		if(opt.target) {
			_search.init();
		}
	}
}
olivetree.init();